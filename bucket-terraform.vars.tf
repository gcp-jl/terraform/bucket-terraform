variable "name" {
	description = "Bucket's Name"
	type = string
}

variable "labels" {
	description = "Labels to indetifier the resource"
	type = map
	default = {
		owner = "jeyseven"
		tier  = "test"
		object = "bucket"
	}
}
