This bucket was created to store the project's tfstate.

Once created, create a tf file with the following format

<pre>
terraform { 
	backend  "gcs" { 
		bucket = ""  ###< --- Put here the name of the created bucket  
		prefix = "testing/terraform.tfstate" 
	} 
}
</pre>
Info: https://www.terraform.io/docs/backends/config.html