resource "google_storage_bucket" "state-terraform" {
	name	= var.name
	project = var.project
	location = "US"
	bucket_policy_only = true
	labels = var.labels

	versioning {
		enabled = true
	}
}
