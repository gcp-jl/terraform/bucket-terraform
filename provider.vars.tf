variable "project" {
    description = "Project's Id"
    type = string
    default = "vaulted-broker-262513"
}   
variable "region" {
    description = "Region where the object will be"
    type = string
    default = "us-central1"
}
variable "zone" {
    description = "Zone Selected as default"
    type = string
    default = "us-central1-a"
}   
